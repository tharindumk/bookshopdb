import java.io.*;
import java.util.*;

class bookrecords
{
    private string fname;
    private string surname;
    private int isbn;

    public void setFname(string fname) {
        this.fname = fname;
    }

    public void setSurname(string surname) {
        this.surname = surname;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public string getFname() {
        return fname;
    }

    public string getSurname() {
        return surname;
    }

    public int getIsbn() {
        return isbn;
    }
    
    public void PrintRecord()
         
 {
     System.out.println("Book Title is: "+title +"    "+"ISBN:"  +isbn+"   " +"Surname is:"+surname+"    "+"Fname is:"+fname);
 }
    
    
    public boolean matchTitle(String ti)
 {
     boolean matched = false;
     
     Pattern patt = Pattern.compile(ti);
     Matcher mat = patt.matcher(title);
     
     while(mat.find())
     {
         matched = true;
         break;
     }
     
     
     return matched;
     
   }
    
 
}
    
class Node
{
    public int bookrecord;
    public Node leftChild;
    public Node rightChild;
    public void displayNode()
    {
        System.out.print("[");
        System.out.print(item);
        System.out.print("]");
    }
    
     public Node(String keyname,BookRecord record)

    {
        key=keyname;
        bookrecord=record;
    }
}

class Tree
{
    private Node root;
    public Tree()
            
    { root = null; }
    
    /*in hear we are finding the existing title with the current node*/
    
    public BookRecord find(String title)
    {
        if(root == null)
            return null;

        Node current = root;
        while(!current.bookrecord.getTitle().equals(title))
        {
             /*if the compared title is less than 0 then current node will equal to left child*/
            
            if(current.bookrecord.getTitle().compareTo(title) < 0)
                current = current.leftChild;
            
           /*current node will be right*/
            
            else
                current = current.rightChild;
                
            if(current == null)
                
               
               return null;
           
        }
       
        return current.bookrecord;
    } 
    
    public void insert(int id)
    {
        Node newNode = new Node();
        newNode.item = id;
        if(root==null)
            root = newNode;
        else
        {
            Node current = root;
            Node parent;
            int key;
            while(true)
            {
                parent = current;
                if(id < current.item)
                {
                   key = bookRecord.getTitle();
                    if(key.compareTo(current.key) < 0 )
                    {
                        parent.leftChild = newNode;
                        return;
                    }
                } 
                else
                {
                    current = current.rightChild;
                    if(current == null) 
                    {
                        parent.rightChild = newNode;
                        return;
                    }
                } 
            } 
        } 
    } 
    public void deleteByName(String keyword){
		
		root = delete(root ,keyword);
	}
	
	private Node delete(Node node, String key) {
		if (node == null) return null;
		/* Finding the node that need to be deleted*/
		int comparator = key.compareTo(node.key);
		if      (comparator < 0) 
			node.leftChild  = delete(node.leftChild,  key);
		else if (comparator > 0) 
			node.rightChild = delete(node.rightChild, key);
		else { 
		/*  3 cases when deleting node from the Binary search tree*/
			if (node.rightChild == null) 
				return node.leftChild;
			if (node.leftChild  == null) 
				return node.rightChild;
			Node t = node;
			/* Find the minimum node from right sub tree of the deleting node */
			node = min(t.rightChild);
			/*Replacing deleting node with minimum node that found above */
			node.rightChild = deleteMin(t.rightChild);
			node.leftChild = t.leftChild;
		} 

		return node;
	}
    /*removing the minimum node from the given tree and returning the root of new tree */
        
	private Node deleteMin(Node node) {
		if (node.leftChild == null) return node.rightChild;
		node.leftChild = deleteMin(node.leftChild);
		return node;
	}
	/*gives the reference of minimum node in the tree */
        
	private Node min(Node node) { 
		if (node.leftChild == null) return node; 
		else                
			return min(node.leftChild); 
	}
    
    
       public void print(String keyword)
    {
        printBookRecord(root ,keyword);
    }

    public void printBookRecord(Node node ,String keyWord)
    {
      if(node == null)  return;
      
      printBookRecord(node.leftChild,keyWord);
      
      if(node.bookrecord.matchTitle(keyWord))
      {
          node.bookrecord.PrintRecord();
      }
       printBookRecord(node.rightChild,keyWord);
     }
    
    
    public static void main(String[] args)
    {
        Tree bst = new Tree();

       BookRecord record = new BookRecord();
       
       BookRecord record1 = new BookRecord();
        
               

        record.setIsbn(12355);
        record.setTitle("Java");
        record.setFname("anushka");
        record.setSurname("Dharmapala");
        bst.insert(record);  
       
       
      //bst.deleteByName("Java");
       
            
        
     bst.print("Java");
      //bst.print("PHP");
     
     
    
     bst.find("Java").toString();       
        
        System.out.println("");
        
        
       
       
       
        
    
       
    }
}

