class BookRecord
{
   
    private String title;
    private String fname;
    private String surname;
    private int isbn;
    
    public void setTitle(String title) {
        this.title = title;
    }


    public void setFname(String fname) {
        this.fname = fname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public String getFname() {
        return fname;
    }

    public String getSurname() {
        return surname;
    }

    public int getIsbn() {
        return isbn;
    }
        public String getTitle() {
        return title;
    }
   

    //write getter and setter methods for book title */
 
}