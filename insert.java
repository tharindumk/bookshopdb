 public void insert(BookRecord record)
    {
        /*new Node() changed */
        Node newNode = new Node(record.getTitle(),record);
        /*newNode.item = id; changed */
        newNode.bookrecord = record;
        if(root==null)
            root = newNode;
        else
        {
            Node current = root;
            Node parent;
            int key;
            while(true)
            {
                parent = current;
                /* (id < current.item) changed */
                if(record.getTitle().compareTo(parent.key) < 0)
                {
                    current = current.leftChild;
                    if(current == null) 
                    {
                        parent.leftChild = newNode;
                        return;
                    }
                } 
                else if(record.getTitle().compareTo(parent.key) > 0)
                {
                    current = current.rightChild;
                    if(current == null) 
                    {
                        parent.rightChild = newNode;
                        return;
                    }
                } 
            } 
        } 
    } 